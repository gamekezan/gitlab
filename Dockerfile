FROM centos:7

RUN yum update -y && yum install -y python3-pip make gcc openssl-devel bzip2-devel libffi-devel
RUN curl -O https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tgz
RUN tar -xzf Python-3.8.1.tgz
RUN cd Python-3.8.1/ && ./configure --enable-optimizations && make altinstall
RUN python3.8 -m pip install flask flask_restful flask_jsonpify

ADD python-api.py /python_api/python-api.py

ENTRYPOINT ["python3.8", "/python_api/python-api.py"]
